# Jupyter Notebook prototypes to expore data.4TU API

* [API-read-dataset.ipynb](API-read-dataset.ipynb) - Get information on datasets 
* [API-upload-file.ipynb](API-upload-file.ipynb) - WIP - upload file to dataset



## requirements
**Python dependencies**: see [requirement.txt](requirements.txt)

**4TU API token**:
* get it from https://data.4tu.nl/my/sessions/new
* add it to `.env` file:

```
API_TOKEN=yourtoken
```

